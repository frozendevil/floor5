//
//  main.m
//  Floor5
//
//  Created by Izzy Fraimow on 10/6/10.
//  Copyright 2010 Anathema Calculus. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc,  (const char **) argv);
}
