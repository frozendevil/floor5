//
//  Floor5AppDelegate.m
//  Floor5
//
//  Created by Izzy Fraimow on 10/6/10.
//  Copyright 2010 Anathema Calculus. All rights reserved.
//

#import "Floor5AppDelegate.h"

#import <Carbon/Carbon.h>
#import <AudioToolbox/AudioServices.h>

id refToSelf; // used to support FrontWindowSwitchedCallback

@interface Floor5AppDelegate () {
	NSStatusItem *statusItem;
	pid_t currentPid;
	
	QTMovie *movie;
	
	BOOL stopped;
}

- (void)frontApplicationSwitched;
//- (void)makeProcessTrusted;
- (void)askUserToEnableAccessForAssistiveDevices;

- (NSArray *)subelementsFromElement:(AXUIElementRef)element forAttribute:(NSString *)attribute;
- (void)playSong;
- (void)stopSong;
@end

@implementation Floor5AppDelegate

static OSStatus AppFrontSwitchedHandler(EventHandlerCallRef inHandlerCallRef, EventRef inEvent, void *selfInstance)
{
    [(Floor5AppDelegate *)selfInstance frontApplicationSwitched];
    return 0;
}

#define getError()																			\
do {																						\
	error = AXUIElementCopyAttributeValue(axElement, kAXRoleAttribute, (CFTypeRef *)&role); \
} while(false)

#define checkError(error)																	\
do {																						\
    if (error != kAXErrorSuccess) {															\
        [appDelegate stopSong];																\
        return;																				\
    }																						\
} while(false)

static void SearchWindowForProgressBar(AXUIElementRef theWindow, void *selfInstance)
{	
    Floor5AppDelegate *appDelegate = (Floor5AppDelegate *)selfInstance;
    
	NSArray *windowChildren = nil;
	AXError error = AXUIElementCopyAttributeValues(theWindow, kAXChildrenAttribute, 0, 100, (CFArrayRef *)&windowChildren);
	CFMakeCollectable(windowChildren);
    
    checkError(error);
		
	BOOL progressBarFound = NO;
	
	NSEnumerator *childrenEnumerator = [windowChildren objectEnumerator];
	AXUIElementRef axElement;
	NSString *role;
    // we're using the enumerator here (instead of for-each syntax) because it's easier/cleaner
    // to cast the result of -nextObject to the right type this way.
	while (axElement = (AXUIElementRef)[childrenEnumerator nextObject]) {
		getError();
		checkError(error);
        
		if ([role isEqualToString:@"AXScrollArea"]) 
			break;
	}
	
	NSArray *scrollChildren = nil;
	error = AXUIElementCopyAttributeValues(axElement, kAXChildrenAttribute, 0, 100, (CFArrayRef *)&scrollChildren);
	CFMakeCollectable(scrollChildren);
	
    checkError(error);
	
	childrenEnumerator = [scrollChildren objectEnumerator];
	while (axElement = (AXUIElementRef)[childrenEnumerator nextObject]) {
		getError();
		checkError(error);
		
		if ([role isEqualToString:@"AXProgressIndicator"] || [role isEqualToString:@"AXBusyIndicator"]) {
			progressBarFound = YES;
			break;
		}
	}
	
	if(progressBarFound) {
		[appDelegate playSong];
	} else {
		[appDelegate stopSong];
	}
}

static void GetFrontWindow(AXUIElementRef app, void *inUserData)
{
	AXUIElementRef frontWindow = NULL;
	AXError err = AXUIElementCopyAttributeValue(app, kAXMainWindowAttribute, (CFTypeRef*)&frontWindow);
	
	if(kAXErrorSuccess != err) return;
	
	SearchWindowForProgressBar(frontWindow, inUserData);
}

static void FrontWindowSwitchedCallback(AXObserverRef observer, AXUIElementRef element, CFStringRef notification, void *refcon)
{
	SearchWindowForProgressBar(element, refToSelf);
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
	refToSelf = self;
	
	if(!AXAPIEnabled() && !AXIsProcessTrusted())
	{
		//NSRunAlertPanel(@"Oh No!!", @"Access for assistive devices is not enabled and we are not trusted. Please trust this app and relaunch it.", @"Make Process Trusted", nil, nil);
		//[self makeProcessTrusted];
		[self askUserToEnableAccessForAssistiveDevices];
	}
	
	EventTypeSpec spec = { kEventClassApplication,  kEventAppFrontSwitched };
    OSStatus err = InstallApplicationEventHandler(NewEventHandlerUPP(AppFrontSwitchedHandler), 1, &spec, (void*)self, NULL);
	
    if (err)
        NSLog(@"Could not install event handler");
	
	NSURL *movieURL = [[NSBundle mainBundle] URLForResource:@"elevator_music" withExtension:@"mp3"];

	movie = [[QTMovie alloc] initWithURL:movieURL error:nil];
	stopped = YES;
	
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(movieDidEnd:) name:QTMovieDidEndNotification object:nil];
}

- (void)awakeFromNib {
	statusItem = [[[NSStatusBar systemStatusBar] statusItemWithLength:NSVariableStatusItemLength] retain];
	[statusItem setMenu:statusMenu];
	NSImage *menuIcon = [NSImage imageNamed:@"75px-Elevator_icon.png"];
	NSImage *menuIconHilite = [NSImage imageNamed:@"75px-Elevator_icon_inverted.png"];
	[statusItem setImage:menuIcon];
	[statusItem setAlternateImage:menuIconHilite];
	[statusItem setHighlightMode:YES];
}

- (void)askUserToEnableAccessForAssistiveDevices; {
	int result = NSRunAlertPanel(@"Enable Access for Assistive Devices." , @"To continue, please enable access for assistive devices in the Universal Access pane in System Preferences. Then, relaunch the application." , @"Open System Preferences", @"Quit", nil);
	
	if(result == NSAlertDefaultReturn) {
		[[NSWorkspace sharedWorkspace] openFile:@"/System/Library/PreferencePanes/UniversalAccessPref.prefPane"];
	}
	
	[NSApp terminate:nil];
}

- (void)frontApplicationSwitched; {	
	pid_t process_pid = [[[[NSWorkspace sharedWorkspace] activeApplication] objectForKey:@"NSApplicationProcessIdentifier"] longValue];
		
	AXUIElementRef element = AXUIElementCreateApplication(process_pid);
	
	AXObserverRef observer;
	AXObserverCreate(process_pid, FrontWindowSwitchedCallback, &observer);
	AXObserverAddNotification(observer, element, kAXFocusedWindowChangedNotification, NULL);
	
	CFRunLoopAddSource([[NSRunLoop currentRunLoop] getCFRunLoop],
					   AXObserverGetRunLoopSource(observer),
					   kCFRunLoopDefaultMode );
	
	GetFrontWindow(element, (void *)self);

}

- (NSArray *)subelementsFromElement:(AXUIElementRef)element forAttribute:(NSString *)attribute; {
	NSArray *subElements = nil;
	CFIndex count = 0;
	AXError result;
	
	result = AXUIElementGetAttributeValueCount(element, (CFStringRef)attribute, &count);
	if (result != kAXErrorSuccess) return nil;
	
	result = AXUIElementCopyAttributeValues(element, (CFStringRef)attribute, 0, count, (CFArrayRef *)&subElements);
	if (result != kAXErrorSuccess) return nil;
	
	return [subElements autorelease];
}

- (void)playSong; {
	[movie play];
	stopped = NO;
}

- (void)stopSong; {
	[movie stop];
	stopped = YES;
}

- (void)movieDidEnd:(NSNotification *)note {
	[movie gotoBeginning];
	NSLog(@"GOING BACK TO THE BEGINNING!!!");
}

@end
