//
//  Floor5AppDelegate.h
//  Floor5
//
//  Created by Izzy Fraimow on 10/6/10.
//  Copyright 2010 Anathema Calculus. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <Security/Authorization.h>
#import <Security/AuthorizationTags.h>
#import <QTKit/QTKit.h>

@interface Floor5AppDelegate : NSObject <NSApplicationDelegate> {
	IBOutlet NSMenu *statusMenu;
}


@end
