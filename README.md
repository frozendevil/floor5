**Floor5**
==========
*An ugly, disgusting, hack that uses the accessability API to play elevator music when the front-most window contains a progress bar*

**Notes:**

- The music file must be supplied by you and integrated into the project by hand. (hint: name it elevator_music.mp3 and just drag it right on in there.)
- It may or may not work under Lion.
